#include "TFile.h"
#include "TGraphAsymmErrors.h"

// Gaudi
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/IEventProcessor.h"
#include "GaudiUtils/HistoLabels.h"

// Tb/TbKernel
#include "TbKernel/TbFunctors.h"

// Tb/TbEvent
#include "Event/TbTrack.h"
#include "Event/TbCluster.h"

// Tb/TbKernel
#include "TbKernel/TbConstants.h"
#include "TbKernel/TbModule.h"

// Local
#include "TbFlEfficiency.h"

using namespace Gaudi::Utils::Histos;

DECLARE_COMPONENT( TbFlEfficiency )

//=============================================================================
// Standard constructor
//=============================================================================
TbFlEfficiency::TbFlEfficiency(const std::string& name,
                               ISvcLocator* pSvcLocator)
    : TbAlgorithm(name, pSvcLocator) {

  declareProperty("TrackLocation",
                  m_trackLocation = LHCb::TbTrackLocation::Default);
  declareProperty("ClusterLocation",
                  m_clusterLocation = LHCb::TbClusterLocation::Default);

  declareProperty("CheckHitAlivePixel", m_checkHitAlivePixel = true);
  declareProperty("PointingResAllowanceDeadPixel",
                  m_pointingResAllowance_deadPixels = 1);
  declareProperty("TakeDeadPixelsFromFile", m_takeDeadPixelsFromFile = false);

  declareProperty("nTotalTracks", m_nTotalTracks = 0);
  declareProperty("MaxChi2", m_maxChi2 = 0);
  declareProperty("TrackFitTool", m_trackFitTool = "TbTrackFit");

  declareProperty("TimeWindow", m_twindow = 100. * Gaudi::Units::ns);
  declareProperty("XWindow", m_xwindow = 0.15 * Gaudi::Units::mm);

  declareProperty("FiducialMinX", m_minX = -20. * Gaudi::Units::mm);
  declareProperty("FiducialMaxX", m_maxX =  20. * Gaudi::Units::mm);
  declareProperty("FiducialMinY", m_minY = -20. * Gaudi::Units::mm);
  declareProperty("FiducialMaxY", m_maxY =  20. * Gaudi::Units::mm);
}

//=============================================================================
// Initialization
//=============================================================================
StatusCode TbFlEfficiency::initialize() {
  // Initialise the base class.
  StatusCode sc = TbAlgorithm::initialize();
  if (sc.isFailure()) return sc;
  // Setup the track fit tool.
  m_trackFit = tool<ITbTrackFit>(m_trackFitTool, "Fitter", this);
  if (!m_trackFit) return Error("Cannot retrieve track fit tool.");

  setupPlots();
  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode TbFlEfficiency::execute() {

  // Grab the clusters.
  std::vector<const LHCb::TbClusters*> clusters(m_nPlanes, nullptr);
  for (unsigned int i = 0; i < m_nPlanes; ++i) {
    const std::string clusterLocation = m_clusterLocation + std::to_string(i);
    clusters[i] = getIfExists<LHCb::TbClusters>(clusterLocation);
    if (!clusters[i]) return Error("No clusters in " + clusterLocation);
    // Count the number of (associated) clusters in the fiducal region.
    unsigned int nTrackedClusters = 0;
    unsigned int nClusters = 0;
    for (const LHCb::TbCluster* cluster : *clusters[i]) {
      if (cluster->x() < m_minX || cluster->x() > m_maxX ||
          cluster->y() < m_minY || cluster->y() > m_maxY) {
        continue;
      }
      ++nClusters;
      if (cluster->associated()) ++nTrackedClusters;
    }
    m_hNClustersTracked->fill(i, nTrackedClusters);
    m_hNClustersTotal->fill(i, nClusters);
  }

  // Grab the tracks.
  LHCb::TbTracks* tracks = getIfExists<LHCb::TbTracks>(m_trackLocation);
  if (!tracks) return Error("No tracks in " + m_trackLocation);

  // Loop over the tracks.
  for (LHCb::TbTrack* track : *tracks) {
    if (m_maxChi2 > 0 && track->chi2PerNdof() > m_maxChi2) continue;
    fillEfficiency(track, clusters);
  }
  if (m_nTotalTracks != 0 && m_nTracksConsidered >= m_nTotalTracks) {
    SmartIF<IEventProcessor> app(serviceLocator()->service("ApplicationMgr"));
    if (app) app->stopRun();
  }

  return StatusCode::SUCCESS;
}

//=============================================================================
// Fill efficiency distributions.
//=============================================================================
void TbFlEfficiency::fillEfficiency(LHCb::TbTrack* track,
                                    const std::vector<const LHCb::TbClusters*>& clusters) {

  std::vector<bool> foundCluster(m_nPlanes, false);
  const auto trclusters = track->clusters();
  unsigned int counter = 0;
  for (auto it = trclusters.cbegin(), end = trclusters.cend(); it != end;
       ++it) {
    const unsigned int plane = (*it)->plane();
    foundCluster[plane] = true;
    ++counter;
  }
  m_hNClustersPerTrack->fill(counter);

  // Define the time window for associating clusters to this track.
  const double tMin = track->htime() - m_twindow;
  const double tMax = track->htime() + m_twindow;

  bool acceptTrack = false;
  for (unsigned int plane = 0; plane < m_nPlanes; ++plane) {
    if (foundCluster[plane]) {
      if (msgLevel(MSG::DEBUG)) {
        debug() << "Plane " << plane << " is in the track: masking" << endmsg;
      }
      // Mask the plane to calculate the unbiased efficiency.
      m_trackFit->maskPlane(plane);
      m_trackFit->fit(track);
      // Re-include the plane under consideration in the fit
      m_trackFit->unmaskPlane(plane);
    }

    // Find the track intercept with the plane masked.
    const auto interceptUG = geomSvc()->intercept(track, plane);
    const auto interceptUL = geomSvc()->globalToLocal(interceptUG, plane);
    // Make sure the track crosses the active area of the plane.
    if (interceptUL.x() < 0. || interceptUL.x() > 14.08) continue;
    if (interceptUL.y() < 0. || interceptUL.y() > 14.08) continue;
    // Make sure the track intercept is in the fiducial region.
    if (interceptUG.x() < m_minX || interceptUG.x() > m_maxX) continue;
    if (interceptUG.y() < m_minY || interceptUG.y() > m_maxY) continue;

    const int row = interceptUL.y() / Tb::PixelPitch - 0.5;
    const int col = interceptUL.x() / Tb::PixelPitch - 0.5;

    // Makes sure the track doesn't cross a masked or dead pixel.
    if (m_checkHitAlivePixel && !passedThroughAlivePixel(col, row, plane)) {
      continue;
    }
    acceptTrack = true;

    // Calculate the unbiased efficiency.
    unsigned int nClustersAssociated = 0;

    // Get the first cluster within the time window.
    auto end = clusters[plane]->end();
    auto begin = std::lower_bound(clusters[plane]->begin(), end, tMin,
                                  lowerBound<const LHCb::TbCluster*>());

    int size = 0;
    for (auto it = begin; it != end; ++it) {
      if ((*it)->htime() > tMax) continue;
      if ((*it)->htime() < tMin) continue;

      const double dx = (*it)->x() - interceptUG.x();
      const double dy = (*it)->y() - interceptUG.y();
      if (fabs(dx) > m_xwindow || fabs(dy) > m_xwindow) continue;
      nClustersAssociated++;
      if (nClustersAssociated == 1) size = (*it)->size();
    }
    const bool pass = nClustersAssociated > 0;

    const double xIntra = fmod(interceptUL.x(), Tb::PixelPitch);
    const double yIntra = fmod(interceptUL.y(), Tb::PixelPitch);
    const double xIntra2 = fmod(interceptUL.x(), 2 * Tb::PixelPitch);
    const double yIntra2 = fmod(interceptUL.y(), 2 * Tb::PixelPitch);

    m_hNClustersAssociated[plane]->fill(nClustersAssociated);

    m_hPerPlaneTotal->fill(plane);
    m_hColTotal[plane]->fill(col);
    m_hRowTotal[plane]->fill(row);
    m_hColRowTotal[plane]->fill(col, row);
    m_hIntraTotal[plane]->fill(xIntra, yIntra);
    m_hIntra2x2Total[plane]->fill(xIntra2, yIntra2);

    if (pass) {
      m_hClusterSize[plane]->fill(size);
      m_hPerPlanePass->fill(plane);
      m_hColPass[plane]->fill(col);
      m_hRowPass[plane]->fill(row);
      m_hColRowPass[plane]->fill(col, row);
      m_hIntraPass[plane]->fill(xIntra, yIntra);
      m_hIntra2x2Pass[plane]->fill(xIntra2, yIntra2);
      if (size == 1)
        m_hIntraPassCls1[plane]->fill(xIntra, yIntra);
      if (size == 2)
        m_hIntraPassCls2[plane]->fill(xIntra, yIntra);
      if (size == 3)
        m_hIntraPassCls3[plane]->fill(xIntra, yIntra);
      if (size == 4)
        m_hIntraPassCls4[plane]->fill(xIntra, yIntra);
    }
    // Refit the track.
    m_trackFit->fit(track);
  }
  if (acceptTrack) ++m_nTracksConsidered;
}

//=============================================================================
// Setup plots
//=============================================================================
void TbFlEfficiency::setupPlots() {

  double low = -0.5;
  double high = m_nPlanes - 0.5;
  unsigned int bins = m_nPlanes;
  std::string name = "Global";
  m_hPerPlaneTotal = book1D(name + "/Total", name, low, high, bins);
  m_hPerPlanePass = book1D(name + "/Pass", name, low, high, bins);
  setAxisLabels(m_hPerPlaneTotal, "plane", "entries");
  setAxisLabels(m_hPerPlanePass, "plane", "entries");

  name = "NClusters/Total";
  m_hNClustersTotal = book1D(name, "Number of clusters", low, high, bins);
  name = "NClusters/Pass";
  m_hNClustersTracked = book1D(name, "Associated clusters", low, high, bins);
  setAxisLabels(m_hNClustersTotal, "plane", "number of clusters");
  setAxisLabels(m_hNClustersTracked, "plane", "number of associated clusters");

  low = 0.5;
  high = m_nPlanes + 0.5;
  name = "NClustersPerTrack";
  m_hNClustersPerTrack = book1D(name, name, low, high , bins);
  setAxisLabels(m_hNClustersPerTrack,"N Clusters", "entries");

  for (unsigned int i = 0; i < m_nPlanes; ++i) {
    const std::string plane = std::to_string(i);
    const std::string title = geomSvc()->modules().at(i)->id();

    low = -0.5;
    high = 255.5;
    bins = 256;
    name = "Col/Plane" + plane + "/Total";
    m_hColTotal.push_back(book1D(name, title, low, high, bins));
    name = "Row/Plane" + plane + "/Total";
    m_hRowTotal.push_back(book1D(name, title, low, high, bins));
    name = "Col/Plane" + plane + "/Pass";
    m_hColPass.push_back(book1D(name, title, low, high, bins));
    name = "Row/Plane" + plane + "/Pass";
    m_hRowPass.push_back(book1D(name, title, low, high, bins));
    setAxisLabels(m_hColTotal[i], "column", "entries");
    setAxisLabels(m_hRowTotal[i], "row", "entries");
    setAxisLabels(m_hColPass[i], "column", "entries");
    setAxisLabels(m_hRowPass[i], "row", "entries");

    name = "ColRow/Plane" + plane + "/Total";
    m_hColRowTotal.push_back(book2D(name, title, low, high, bins, low, high, bins));
    name = "ColRow/Plane" + plane + "/Pass";
    m_hColRowPass.push_back(book2D(name, title, low, high, bins, low, high, bins));
    setAxisLabels(m_hColRowTotal[i], "column", "row");
    setAxisLabels(m_hColRowPass[i], "column", "row");

    low = 0.;
    high = Tb::PixelPitch;
    bins = 32;
    name = "IntraPixel/Plane" + plane + "/Total";
    m_hIntraTotal.push_back(book2D(name, title, low, high, bins, low, high, bins));
    name = "IntraPixel/Plane" + plane + "/Pass";
    m_hIntraPass.push_back(book2D(name, title, low, high, bins, low, high, bins));
    name = "IntraPixel/Plane" + plane + "/PassCls1";
    m_hIntraPassCls1.push_back(book2D(name, title, low, high, bins, low, high, bins));
    name = "IntraPixel/Plane" + plane + "/PassCls2";
    m_hIntraPassCls2.push_back(book2D(name, title, low, high, bins, low, high, bins));
    name = "IntraPixel/Plane" + plane + "/PassCls3";
    m_hIntraPassCls3.push_back(book2D(name, title, low, high, bins, low, high, bins));
    name = "IntraPixel/Plane" + plane + "/PassCls4";
    m_hIntraPassCls4.push_back(book2D(name, title, low, high, bins, low, high, bins));
    setAxisLabels(m_hIntraTotal[i], "#it{x} [mm]", "#it{y} [mm]");
    setAxisLabels(m_hIntraPass[i], "#it{x} [mm]", "#it{y} [mm]");
    setAxisLabels(m_hIntraPassCls1[i], "#it{x} [mm]", "#it{y} [mm]");
    setAxisLabels(m_hIntraPassCls2[i], "#it{x} [mm]", "#it{y} [mm]");
    setAxisLabels(m_hIntraPassCls3[i], "#it{x} [mm]", "#it{y} [mm]");
    setAxisLabels(m_hIntraPassCls4[i], "#it{x} [mm]", "#it{y} [mm]");

    high = 2 * Tb::PixelPitch;
    bins = 64;
    name = "IntraPixel2x2/Plane" + plane + "/Total";
    m_hIntra2x2Total.push_back(book2D(name, title, low, high, bins, low, high, bins));
    name + "IntraPixel2x2/Plane" + plane + "/Pass";
    m_hIntra2x2Pass.push_back(book2D(name, title, low, high, bins, low, high, bins));
    setAxisLabels(m_hIntra2x2Total[i], "#it{x} [mm]", "#it{y} [mm]");
    setAxisLabels(m_hIntra2x2Pass[i], "#it{x} [mm]", "#it{y} [mm]");

    name = "ClusterSize/Plane" + plane;
    m_hClusterSize.push_back(book1D(name, title, -0.5, 20.5, 21));
    setAxisLabels(m_hClusterSize[i], "cluster size", "entries");

    name = "NClustersAssociated/Plane" + plane;
    m_hNClustersAssociated.push_back(book1D(name, title, -0.5, 99.5, 100));
    setAxisLabels(m_hNClustersAssociated[i], "number of candidates", "entries");
  }
}

//=============================================================================
// Determine whether a track passes (close to) a masked or dead pixel.
//=============================================================================
bool TbFlEfficiency::passedThroughAlivePixel(const int col, const int row,
                                             const unsigned int plane) const {

  const int col0 = col - m_pointingResAllowance_deadPixels;
  const int col1 = col + m_pointingResAllowance_deadPixels + 1;
  const int row0 = row - m_pointingResAllowance_deadPixels;
  const int row1 = row + m_pointingResAllowance_deadPixels + 1;

  for (int i = row0; i < row1; ++i) {
    if (i < 0 || i > 255) continue;
    for (int j = col0; j < col1; ++j) {
      if (j < 0 || j > 255) continue;
      if (pixelSvc()->isMasked(pixelSvc()->address(j, i), plane)) {
        counter("nTracksStrikingMaskedPixels") += 1;
        return false;
      /*
      } else if (m_takeDeadPixelsFromFile &&
                 m_deadPixelMap->GetBinContent(j, i) == 0) {
        counter("nTracksStrikingDeadPixels") += 1;
        return false;
      */
      }
    }
  }
  return true;
}
