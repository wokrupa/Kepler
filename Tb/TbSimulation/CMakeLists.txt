################################################################################
# Package: TbSimulation
################################################################################
gaudi_subdir(TbSimulation v1r4)

gaudi_depends_on_subdirs(Tb/TbEvent
                         Tb/TbKernel
                         GaudiAlg)

find_package(ROOT COMPONENTS MathCore)

find_package(Boost)
find_package(ROOT)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

gaudi_add_module(TbSimulation
                 src/*.cpp
                 LINK_LIBRARIES TbEventLib TbKernelLib GaudiAlgLib ROOT)

